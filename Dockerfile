# Step 1: builder image
# We're using musl to generate smaller images based on Alpine Linux
# Base image: https://github.com/emk/rust-musl-builder
FROM docker.io/library/rust:alpine as builder

# This Dockerfile contains two `cargo build` commands;
# The first compiles the dependencies of the application
# while the second one compiles and links the app itself, which
# changes less often than the dependencies.
# Inspired from
# https://blog.logrocket.com/packaging-a-rust-web-service-using-docker/

RUN apk update && apk add --no-cache openssl-dev musl-dev

# Compile dependencies
RUN USER=root cargo new --bin rust-fortune
WORKDIR ./rust-fortune
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml
RUN cargo build --release

# Compile the app itself
RUN rm src/*.rs
RUN rm ./target/release/deps/rust_fortune*
ADD src ./src
ADD templates ./templates
RUN cargo build --release

# tag::production[]
# Step 2: runtime image
FROM docker.io/library/alpine:3.17

# Add dependencies
ENV TZ=Etc/UTC
RUN apk update \
    && apk add --no-cache fortune ca-certificates tzdata \
    && rm -rf /var/cache/apk/*

# Copy self-contained executable
COPY --from=builder /rust-fortune/target/release/rust-fortune /usr/local/bin/rust-fortune

EXPOSE 8080

# Never run as root
# <1>
USER 1001:0

CMD ["/usr/local/bin/rust-fortune"]
# end::production[]
