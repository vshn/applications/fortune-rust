use actix_web::{get, App, HttpRequest, HttpResponse, HttpServer, Responder};
use askama::Template;
use hostname;
use rand::Rng;
use serde::Serialize;
use std::process::Command;

#[derive(Template)]
#[template(path = "fortune.html")]
#[derive(Serialize)]
struct FortuneData {
    number: u32,
    message: String,
    version: String,
    hostname: String,
}

impl FortuneData {
    fn new() -> FortuneData {
        let mut rng = rand::thread_rng();
        let command = Command::new("fortune")
            .output()
            .expect("failed to execute process");
        FortuneData {
            message: format!("{}", String::from_utf8_lossy(&command.stdout)),
            number: rng.gen_range(0..1000),
            version: String::from("1.2-rust"),
            hostname: get_hostname(),
        }
    }

    fn to_plain(&self) -> String {
        format!(
            "Fortune {} cookie of the day #{}:\n\n{}",
            self.version, self.number, self.message
        )
    }
}

fn get_hostname() -> String {
    if let Some(hostname) = hostname::get().ok() {
        if let Some(name) = hostname.into_string().ok() {
            return name;
        }
    }
    return String::from("");
}

fn get_content_type<'a>(req: &'a HttpRequest) -> Option<&'a str> {
    req.headers().get("Accept")?.to_str().ok()
}

// tag::router[]
#[get("/")]
async fn fortune(req: HttpRequest) -> impl Responder {
    let response = FortuneData::new();
    if let Some(content_type) = get_content_type(&req) {
        if content_type == "text/plain" {
            return HttpResponse::Ok().body(response.to_plain());
        }
        if content_type == "application/json" {
            return HttpResponse::Ok().json(response);
        }
    }
    HttpResponse::Ok().body(response.render().unwrap())
}
// end::router[]

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().service(fortune))
        .bind("0.0.0.0:8080")?
        .run()
        .await
}
